# buildOpenCV
Build and install OpenCV

These scripts build OpenCV version 4.x.x

OpenCV is a rich environment which can be configured in many different ways. You should configure OpenCV for your needs, by modifying the build file "buildOpenCV.sh". Note that selecting different options in OpenCV may also have additional library requirements which are not included in these scripts. Please read the notes below for other important points before installing.

The buildOpenCV script has two optional command line parameters:

To run the the build file:

```sh
$ ./buildOpenCV.sh
```

This example will build OpenCV in the given file directory and install OpenCV in the /usr/local directory.

The folder ~/opencv and (optional) ~/opencv_contrib contain the source, build and extra data files. If you wish to remove them after installation:

```sh
$ cd opencv/build/
$ sudo make uninstall
```

The libraries will be installed in /usr/lib

Binaries are in /usr/bin

opencv.pc is in /usr/lib/pkgconfig

## Notes
<ul><li>The build process default installation is in /usr/local
Note that the .deb file install into /usr</li>
<li>After installation, the dpkg/apt name does not include version information, e.g. the name is opencv-libs</li>
<li>After installation, create a file called /etc/ld.so.conf.d/opencv.conf and write to it the path to the folder where the binary is stored.For example, I wrote /usr/local/lib/ to my opencv.conf file</li>
<li>Then run the command line as follows:
`$ sudo ldconfig`
</li>
<li>To add OpenCV to virtual env
$ cd ~/venv/lib/python3.6/site-packages/
$ ln -s /usr/local/lib/python3.6/dist-packages/cv2/python-3.6/cv2.cpython-36m-x86_64-linux-gnu.so cv2.so
</li>
</ul>