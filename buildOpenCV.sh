#!/bin/sh
# buildSimpleOpenCV.sh
# Builds and installs simple version of OpenCV
# Including SFM
# OpenCV 4.x.x

sudo apt-add-repository universe
sudo apt-get update

# to support python2
sudo apt-get install python-dev python-numpy -y

# to support python3
sudo apt-get install python3-dev python3-numpy -y

echo "Removing any pre-installed ffmpeg and x264"
sudo apt-get remove ffmpeg x264 libx264-dev

echo "Installing Dependenices"
sudo apt-get install libqt4-dev libtheora-dev libvorbis-dev x264 v4l-utils ffmpeg -y

# Some general development libraries
sudo apt-get install build-essential make cmake cmake-curses-gui g++ pkg-config -y
# libav video input/output development libraries
sudo apt-get install libavformat-dev libavutil-dev libswscale-dev -y
# Video4Linux camera development libraries
sudo apt-get install libv4l-dev -y
# Eigen3 math development libraries
sudo apt-get install libeigen3-dev -y
# OpenGL development libraries (to allow creating graphical windows)
sudo apt-get install libglew1.6-dev -y
# GTK development libraries (to allow creating graphical windows)
sudo apt-get install libgtk2.0-dev -y

sudo apt-get install git libavcodec-dev -y
sudo apt-get install libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libdc1394-22-dev -y

sudo add-apt-repository "deb http://security.ubuntu.com/ubuntu xenial-security main"
sudo apt update
sudo apt install libjasper1 libjasper-dev -y
sudo apt-get install libatlas-base-dev gfortran -y
sudo apt-get install libgtk-3-dev -y

# Install OpenCV dependencies
sudo apt-get -y install \
   checkinstall yasm -y

# Video I/O
sudo apt-get -y install \
  libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev \
  libxine-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev -y

# For building Python wrappers
sudo apt-get -y install \
  python-dev python-numpy -y

# Install sound related libraries
sudo apt-get -y install \
  libfaac-dev libjack-jackd2-dev \
  libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libsdl1.2-dev \
  libva-dev libvdpau-dev  \
  libxvidcore-dev texi2html -y

# Install SFM dependencies
# google-glog + gflags
sudo apt-get install libgoogle-glog-dev -y
sudo apt-get install libgflags-dev -y
# BLAS & LAPACK
sudo apt-get install libatlas-base-dev -y
# Eigen3
sudo apt-get install libeigen3-dev -y
# SuiteSparse and CXSparse (optional)
# - If you want to build Ceres as a *static* library (the default)
#   you can use the SuiteSparse package in the main Ubuntu package
#   repository:
sudo apt-get install libsuitesparse-dev -y
# - However, if you want to build Ceres as a *shared* library, you must
#   add the following PPA:
# sudo add-apt-repository ppa:bzindovic/suitesparse-bugfix-1319687
# sudo apt-get update
# sudo apt-get install libsuitesparse-dev

mkdir ceres-solver && cd ceres-solver
wget -O ceres-solver-1.14.0.tar.gz https://ceres-solver.googlesource.com/ceres-solver/+archive/facb199f3eda902360f9e1d5271372b7e54febe1.tar.gz
tar zxf ceres-solver-1.14.0.tar.gz
mkdir build && cd build
cmake ..
make -j4
make test
sudo make install

# Build OpenCV
NUM_THREADS=4
cd ~/buildopencv
git clone https://github.com/opencv/opencv.git
git clone https://github.com/opencv/opencv_contrib.git
cd opencv
mkdir build
cd build

cmake -D CMAKE_BUILD_TYPE=Release \
    -D OPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules \
    -D BUILD_opencv_python3=yes \
    -D OPENCV_GENERATE_PKGCONFIG=ON \
    -D CMAKE_INSTALL_PREFIX=/usr/local ..
make -j$NUM_THREADS
sudo make install
echo -e "\e[1;32mOpenCV build complete.\e[0m"