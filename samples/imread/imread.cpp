#include <opencv2/opencv.hpp>
#include <unistd.h>
#include <iostream>

using namespace cv;
using namespace std;

int main( int argc, char** argv )
{
	Mat image;
    image = imread(argv[1], IMREAD_COLOR);                  // Read the file
    resize(image, image, cv::Size(), 0.15, 0.15);

    if(! image.data )                                       // Check for invalid input
    {
        cout <<  "Could not open or find the image" << std::endl ;
        return -1;
    }

    cout <<  "Success open image" << std::endl ;
    namedWindow( "Display window", WINDOW_AUTOSIZE );       // Create a window for display.
    imshow( "Display window", image );                      // Show our image inside it.

    waitKey(0);                                             // Wait for a keystroke in the window
    return 0;
}
